import threading  # Подключаем модуль threading для работы с потоками
import os
import re
import logging

class Scanner(object):

    def __init__(self, targetDir, signature):

        """
            Конструктор класса
        """

        self.targetDir = targetDir
        self.signature = signature
    
    def get_logger(self):
        
        """            
            Возвращает логгер для удобной работы с потоками
        """

        logger = logging.getLogger("threading_example")
        logger.setLevel(logging.DEBUG)
    
        fh = logging.FileHandler("logfile.log")
        fmt = '%(asctime)s - %(threadName)s - %(levelname)s - %(message)s'
        formatter = logging.Formatter(fmt)
        fh.setFormatter(formatter)
    
        logger.addHandler(fh)

        return logger

    def getDirectoryFiles(self):
        """            
            Возвращает список всех файлов в указанной директории
        """
        #Массив всех файлов директории и его поддиректорий
        allFiles = []

        os.chdir(self.targetDir)
        for root, dirs, files in os.walk(".", topdown = False):
            for name in files:
                allFiles.append(os.path.join(root, name))
        
        return allFiles
    

    def fileScan(self, file, logger):
        """
            Ищет в файле указанную сигнатуру
        """
        try:
           #Получить содержимое файла (https://python-scripts.com/work-with-files-python)
           #Регулярным выражением проверить содержится ли сигнатура в файле (https://tproger.ru/translations/regular-expression-python/)
           
           with open(file, 'rb') as file_handler:
               for index, line in enumerate(file_handler):
                    #Регулярным выражением ищем совпадение в строке файла
                    result = re.search(bytes(self.signature, 'utf-8'), line)
                    if(result):
                        #Если нашли совпадение, выводим номер строки и имя файла
                        logger.debug("Совпадение в {} строке" . format(index + 1))
                    #print(result.start(), result.group())
                    #print('<'> + file + '> ' + '- detected')
        except:
            print("Ошибка при обработке файла {}, возможно, файл открыт в другой программе!".format(file))

        return

    def searchBySignature(self):
        """
            Ищет указанную сигнатуру во всех файлах директории
        """

        #Создаем логгер для работы с потоками
        logger = self.get_logger()

        #Получаем все файлы директории
        allFiles = self.getDirectoryFiles()

        # Перебор в цикле файлов
        for file in allFiles:
            th = threading.Thread(target=self.fileScan, name=file, kwargs={'file': file, 'logger': logger})  # Создаём поток
            # Запуск потока
            th.start()   
        
        return

 
if __name__ == "__main__":

    print('-' * 40)
    targetDir = input('Введите путь до директории:\n')
    signature = input('Введите сигнатуру поиска:\n')
    print('-' * 40)

    scanner = Scanner(targetDir, signature)
    scanner.searchBySignature()