'use strict';

const handlers = require('./routehandlers');
const { STATUS, REQUEST_ID } = require('./constants');
const MongoClient = require("mongodb").MongoClient;
let crypto = require('crypto');
let net = require('net');

//Храним роуты как константный массив
const ROUTES = ['login', 'logout', 'chat', 'signup'];

//Глобальная переменная сервера
let server = null;

// Объект чата, хранит массив сообщений, а также массив всех подключенных клиентов
let chatObject = {
	messages: [],
	clients: new Map(),
	currentUser: null
};

// Map обработчиков роутов
let handlerNames = new Map();

/*
	Класс, отвечающий за работу сервера
*/
class Server {

	//Конструтор класса для инициализации всего необходимого
	constructor(port) {

		//Подключаемся к базе данных
		this.connectDb();

		this.createServer();

		ROUTES.forEach(route => {
			handlerNames.set(route, route + 'Handler');
		});

	}

	createServer() {

		server = net.createServer((socket) => {

			console.log('Клиент подключился');
			let state = null;

			socket.on('data', async (data) => {
				let request = JSON.parse(data);

				// Достаем по роуту обработчик из map и вызываем его
				let res = await handlers[handlerNames.get(request.route)](this.userCollection, request, chatObject);

				// Формируем ответ для клиента
				state = this.sendResponse(request, res, socket);

				// Если не смогли отправить ответ, выводим ошибку
				if (!state) {
					console.error("Произошла ошибка при отправке ответа клиенту!");
				}

			});


			socket.on('end', function () {
				console.log('Клиент отключился');
			});

		}).listen(1337);
	}

	sendResponse(request, result, socket) {

		let response = null;

		switch (request.requestId) {

			case REQUEST_ID.SIGN_IN:
				if (result === null) {

					// Отправляем статус неавторизован
					response = {
						requestId: REQUEST_ID.SIGN_IN,
						status: STATUS.UNAUTHORIZED
					};

				} else {

					// Формируем session_id как хэш от логина и первых 3 символов пароля.
					let hash_data = request.user.login + request.user.password.substring(0, 3);

					// Получаем session_id
					let session_id = crypto.createHash('md5').update(hash_data).digest("hex");

					// Запоминаем клиента
					chatObject.clients.set(session_id, socket);
					chatObject.currentUser = result.login;

					// Формируем ответ
					response = {
						name: chatObject.currentUser,
						messages: chatObject.messages,
						session_id: session_id,
						requestId: REQUEST_ID.SIGN_IN,
						status: STATUS.SUCCESS
					};

				}

				// Отправляем
				socket.write(JSON.stringify(response));

				return true;

			case REQUEST_ID.SIGN_UP:


				if (result) {

					response = {
						requestId: REQUEST_ID.SIGN_UP,
						status: STATUS.SUCCESS
					};


				} else {

					response = {
						requestId: REQUEST_ID.SIGN_UP,
						status: STATUS.ERROR
					};
				}

				// Отправляем
				socket.write(JSON.stringify(response));

				return result;

			case REQUEST_ID.LOGOUT:

				this.broadcast('<SERVER>', `Клиент ${chatObject.currentUser} отключился`);

				return true;

			case REQUEST_ID.CHAT:

				// Отправляем обновленный messages всем клиентам
				this.broadcast();

				return true;

			default:
				console.error("Что-то пошло не так!");
		}

		return false;
	}

	/*
		Рассылка всем клиентам
	*/
	broadcast(user, message) {
		chatObject.clients.forEach((client) => {

			// Сообщения от сервера
			if (message) {
				let serverMsg = {
					author: user,
					text: message
				};

				chatObject.messages.push(serverMsg);
			}

			let response = {
				name: chatObject.currentUser,
				messages: chatObject.messages,
				requestId: REQUEST_ID.CHAT,
				status: STATUS.SUCCESS
			};

			client.write(JSON.stringify(response));
		});

	}

	/*
		Метод для подключения к базе данных и сохранения коллекций
	*/
	connectDb() {
		const url = "mongodb://localhost:27017/";
		const mongoClient = new MongoClient(url, { useUnifiedTopology: true });

		mongoClient.connect((err, client) => {

			if (err) throw err;

			console.log("Установили соедение с базой данных!");

			const db = client.db("clientDb");

			// Коллекция для хранения пользователей
			this.userCollection = db.collection("users");

			// Коллекция для хранения сообщений чата
			this.msgCollection = db.collection("message");
		});
	}

}

let srv = new Server(5000);