const readline = require('readline');

// Статусы ответов
const STATUS = {
    SUCCESS: 200,
    UNAUTHORIZED: 400,
    ERROR: 500
};

// id запросов
const REQUEST_ID = {
    SIGN_IN: 1,
    SIGN_UP: 2,
    LOGOUT: 3,
    CHAT: 4
};

// Состояние пользователя
const USERSTATE = {
    UNAUTHORIZED: 0,
    AUTHORIZED: 1
};

// Создаем интерфейс для чтения из ввода
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

module.exports = {STATUS, REQUEST_ID, USERSTATE, rl};