'use strict';

let net = require('net');
const { STATUS, REQUEST_ID, USERSTATE, rl } = require('./constants');
const { ActionException } = require('./exceptions');

// Глобальная переменная хранит состояние пользователя
let loginState = USERSTATE.UNAUTHORIZED;

// Глобальная переменная для хранения коннекта клиента
let client;

let clientAuth = {
    name: null,
    session_id: null,
    chatMessages: []
};

/*
    Класс, отвечающий за взаимодействие клиента с сервером.
*/
class Client {

    constructor(port) {

        client = net.connect({ port: port }, () => {
            this.printLog('Успешно соединились с сервером!', false);
            this.startSession();
        });
    }

    startSession() {

        this.printActions();

        // Обработка ответа от сервера
        client.on('data', (data) => {

            let response = JSON.parse(data);

            switch (response.requestId) {

                // Обрабатываем ответ сервера при signin
                case REQUEST_ID.SIGN_IN:

                    // Если сервер вернул 200, значит авторизуем пользователя
                    if (response.status === STATUS.SUCCESS) {

                        loginState = USERSTATE.AUTHORIZED;

                        clientAuth.name = response.name;
                        clientAuth.session_id = response.session_id;
                        clientAuth.chatMessages = response.messages;

                        // Авторизация произошла, перенаправляем в чат
                        this.outMessages();

                        this.printLog("Успешно авторизовались", false);
                        this.printLog("Добро пожаловать в чат!", false);
                        this.printLog("Для вызова меню используйте команду !menu", false);

                        // Читаем заново
                        this.sendMessage();

                        // Берем сообщение от пользователя, отправляем и ожидаем новое сообщение.
                        this.sendMessage();

                    } else if (response.status === STATUS.UNAUTHORIZED) {

                        this.printLog("Пользователя с такими данными не существует!");

                    } else {

                        this.printLog("Возникли некоторые проблемы. Попробуйте позже!");

                    }

                    break;

                // Ответ сервера при signup
                case REQUEST_ID.SIGN_UP:

                    response.status === STATUS.SUCCESS ?
                        this.printLog("Успешно зарегистрировались!") :
                        this.printLog("Возникла ошибка при регистрации. Попробуйте позже!");

                    break;

                case REQUEST_ID.CHAT:

                    clientAuth.chatMessages = response.messages;
                    this.outMessages();
                    this.sendMessage();

                    break;

                default:

                    this.printLog('Что-то пошло не так!', false);
                    break;
            }
        });

    }

    /*
        Метод для вывода сообщений
    */
    printLog(logMsg, actions = true) {
        console.log(logMsg);

        if (actions)
            this.printActions();
    }

    /*
        Метод для вывода сообщений чата
    */
    outMessages() {

        this.cls();

        if (clientAuth.chatMessages.length) {

            clientAuth.chatMessages.forEach(el => {
                console.log(el.author + ': ' + el.text);
            });

            console.log('\n\n\n');

        }

    }

    /*
        Метод очищает окно
    */
    cls() {

        let lines = process.stdout.getWindowSize()[1];

        for (let i = 0; i < lines; i++) {
            console.log('\r\n');
        }

    }

    /* 
        Метод для авторизации на сервере 
    */
    singIn() {
        rl.question('Введите логин: ', (userLogin) => {
            rl.question('Введите пароль: ', (userPass) => {

                // Формируем json-объект для запроса singin к серверу
                let request = {
                    route: 'login',
                    requestId: REQUEST_ID.SIGN_IN,
                    user: {
                        login: userLogin,
                        password: userPass
                    }
                };

                // Отправляем объект серверу
                client.write(JSON.stringify(request));

            });
        });
    }

    /*
        Метод для отправки сообщений пользователя серверу
    */
    sendMessage() {

        rl.question(clientAuth.name + ': ', (message) => {

            // Команда вывода меню для пользователя.
            if (message === '!menu') {
                this.printActions();
                return;
            }

            // Формируем json-объект для запроса к chat на сервере
            let mes = {
                route: 'chat',
                requestId: REQUEST_ID.CHAT,
                message: {
                    author: clientAuth.name,
                    text: message
                }
            };

            // Отправляем запрос
            client.write(JSON.stringify(mes));

        });


    }

    /*
        Метод для вывода меню
    */
    printActions() {

        if (!loginState) {
            console.log(`1. Войти\n2. Зарегистрироваться`);
            this.readAction();
        }
        else {
            console.log(`1. Покинуть чат\n2. Остаться в чате`);
            this.readAction();
        }

    }

    /*
        Метод для чтения действий от пользователя
    */
    readAction() {

        // Если неавторизован выдаем одно меню навигаций
        if (!loginState) {

            // Получаем данные от пользователя и вызываем необходимые действия
            rl.question('Выберите действие: ', (action) => {
                switch (parseInt(action)) {
                    case 1:
                        // Войти на сервер
                        this.singIn();
                        break;
                    case 2:
                        // Регистрация
                        this.singUp();
                        break;
                    default:
                        console.log("\nНеправильно задано действие. Попробуйте заново!");
                        this.printActions();
                }

            });
            // иначе - другое
        } else {
            rl.question('Выберите действие: ', (action) => {
                switch (parseInt(action)) {
                    case 1:
                        // Отключиться от сервера
                        this.logout();
                        break;
                    case 2:

                        this.outMessages();

                        // Остаться в чате
                        this.sendMessage();

                        break;
                    default:
                        console.log("\nНеправильно задано действие. Попробуйте заново!");
                        this.printActions();
                }

            });
        }

    }

    /*
        Метод для регистрации пользователей
    */
    singUp() {

        rl.question('Введите логин: ', (userLogin) => {
            rl.question('Введите пароль: ', (userPass) => {

                // Формируем json-объект для запроса singin к серверу
                let request = {
                    route: 'signup',
                    requestId: REQUEST_ID.SIGN_UP,
                    user: {
                        login: userLogin,
                        password: userPass
                    }
                };

                // Отправляем объект серверу
                client.write(JSON.stringify(request));
            });
        });

    }

    /*
        Метод для отправления запроса серверу на отключение клиента от чата
    */
    logout() {

        // Формируем json-объект для запроса logout к серверу
        let request = {
            route: 'logout',
            requestId: REQUEST_ID.LOGOUT,
            user: {
                name: clientAuth.name,
                session_id: clientAuth.session_id
            }
        };

        //Отправляем запрос на удаление клиента
        client.write(JSON.stringify(request));

        loginState = USERSTATE.UNAUTHORIZED;
        this.cls();
        this.printActions();

    }
}


try {

    let clt = new Client(1337);

} catch (e) {

    //console.log('err', e.message);
    console.log("Произошла ошибка. Попробуйте позже!");

}