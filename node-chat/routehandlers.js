let crypto = require('crypto');

/*
    Обработчик для запроса на регистрацию
*/
async function signupHandler(userCollection, data) {

    return new Promise((resolve, reject) => {

        // Хешируем пароль перед тем как положить в базу
        let user = {
            login: data.user.login,
            password: crypto.createHash('md5').update(data.user.password).digest("hex")
        };

        userCollection.insertOne(user).then(result => {
            resolve(result);
        }).catch(err => {
            console.error(`Возникли какие-то проблемы, попробуйте позже: ${err}`);
            reject(err);
        });
    });

}

/*
    Обработчик для запроса на вход
*/
async function loginHandler(userCollection, data) {

    return new Promise((resolve, reject) => {

        userCollection.findOne({
            $and: [
                { login: data.user.login },
                { password: crypto.createHash('md5').update(data.user.password).digest("hex") },
            ]
        }).then(result => {

            resolve(result);

        }).catch(err => {

            console.error(`Возникли какие-то проблемы, попробуйте позже: ${err}`);
            reject(err);

        });
    });

}

/*
    Обработчик для запроса на выход с сервера
*/
function logoutHandler(userCollection, data, chatObject) {

    // Удаляем элемент Map по ключу при отключении клиента
    chatObject.clients.delete(data.user.session_id);
}

/*
    Обработчик запросов к чату
*/
function chatHandler(userCollection, data, chatObject) {

    //Добавляем сообщение в массив
    chatObject.messages.push(data.message);

}

module.exports = { loginHandler, logoutHandler, chatHandler, signupHandler};
